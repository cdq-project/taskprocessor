FROM eclipse-temurin:21.0.1_12-jdk-alpine

WORKDIR /app

COPY target/TaskProcessor-0.0.1-SNAPSHOT.jar task-processor-1.0.0.jar

EXPOSE 8081

ENTRYPOINT ["java","-jar","task-processor-1.0.0.jar"]

