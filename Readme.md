Getting Started:
To start the application we need to build our target directory with following command in project root directory (requires maven added to environment variables):
mvn clean install

then run the following Docker Compose command in the same directory:
docker-compose up -d

This will bring up all the required components, including RabbitMQ, PostgreSQL, volumes and the application itself.
To change the delay time in the application, modify the SLEEP_TIME field in the Docker Compose file. 
The time is presented in milliseconds (The delay repeats after every iteration during the task processing).




API:

Create Task (POST)
Endpoint:
localhost:8081/tasks

Request Body:
{
    "input": "ABCD",
    "pattern": "BCD"
}

Creates a task that will be processed asynchronously. The service will respond with the Task ID (in UUID format).
If body is not valid, proper information will display.


Get All Tasks (GET)
Endpoint:
localhost:8081/tasks

Responds with all created task IDs.


Get Task by ID (GET)
Endpoint:
localhost:8081/tasks/{id}

Responds with the details of a specific task searched by the given ID. If no task with the given ID is present, a 404 status is returned.





Application Workflow:

The application uses RabbitMQ for asynchronous processing.
Upon receiving a task, it is saved in the database with a PENDING status and an ID is generated.
The task is then sent to RabbitMQ, and the generated ID is returned as the response.
A Consumer method retrieves tasks and starts processing (changing the task status to IN_PROGRESS).
During processing, the task's "progress" field is updated (represented as a percentage from 0 to 100).
When task processing is completed, the status is set to COMPLETED, and the full result is saved in the database.

Users can check task statuses and progress at any time. 
After calling the task while it is being processed, we will see its best result so far. 
However, the proper results will be visible only after task processing is completed.





Application is built using the following tools:

Java 14+
Spring Boot
Hibernate
Maven
Junit 5
Mockito
AssertJ
RabbitMQ
PostgreSQL
Liquibase
Docker
GitLab


