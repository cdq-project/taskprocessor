package com.cdq.taskprocessor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@EntityScan(basePackages = "com.cdq.taskprocessor.model")
public class TaskProcessorApplication {

	public static void main(String[] args) {
		SpringApplication.run(TaskProcessorApplication.class, args);
	}

}
