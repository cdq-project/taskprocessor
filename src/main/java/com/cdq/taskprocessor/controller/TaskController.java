package com.cdq.taskprocessor.controller;

import com.cdq.taskprocessor.model.Task;
import com.cdq.taskprocessor.model.TaskRequest;
import com.cdq.taskprocessor.controller.validation.TaskValidator;
import com.cdq.taskprocessor.service.TaskService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/tasks")
public class TaskController {

    private final TaskValidator validator;
    private final TaskService taskService;

    public TaskController(TaskValidator validator, TaskService taskService) {
        this.validator = validator;
        this.taskService = taskService;
    }

    @PostMapping
    public ResponseEntity<String> createTask(@RequestBody TaskRequest taskRequest) {
        validator.validate(taskRequest);
        Task task = taskService.createTask(taskRequest);
        return ResponseEntity.accepted().body(task.getId());
    }

    @GetMapping
    public ResponseEntity<List<String>> getAllTaskIds() {
        List<String> taskIds = taskService.getAllTaskIds();
        return ResponseEntity.accepted().body(taskIds);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Task> getTaskById(@PathVariable String id) {
        Optional<Task> task = taskService.getTaskById(id);
        return task.map(value -> ResponseEntity.accepted().body(value))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }
}
