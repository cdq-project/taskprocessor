package com.cdq.taskprocessor.controller.validation;

import com.cdq.taskprocessor.model.TaskRequest;
import org.springframework.stereotype.Component;

@Component
public class TaskValidator {

    public void validate(TaskRequest taskRequest) {
        if (taskRequest.getInput() == null || taskRequest.getInput().isEmpty()) {
            throw new RequestValidationException("input field required");
        } else if (taskRequest.getPattern() == null || taskRequest.getPattern().isEmpty()) {
            throw new RequestValidationException("pattern field required");
        } else if (taskRequest.getInput().length() < taskRequest.getPattern().length()) {
            throw new RequestValidationException("input cannot be shorter than pattern");
        }
    }
}
