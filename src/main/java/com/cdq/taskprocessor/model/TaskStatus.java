package com.cdq.taskprocessor.model;

import java.io.Serializable;

public enum TaskStatus implements Serializable {
    PENDING,
    IN_PROGRESS,
    COMPLETED
}
