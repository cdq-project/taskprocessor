package com.cdq.taskprocessor.model.converter;

import com.cdq.taskprocessor.model.Task;
import com.cdq.taskprocessor.model.TaskEntity;
import com.cdq.taskprocessor.model.TaskRequest;
import com.cdq.taskprocessor.model.TaskStatus;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class TaskConverter {

    public TaskEntity toEntity(Task task) {
        TaskEntity taskEntity = new TaskEntity();
        taskEntity.setId(UUID.fromString(task.getId()));
        taskEntity.setInput(task.getInput());
        taskEntity.setPattern(task.getPattern());
        taskEntity.setPosition(task.getPosition());
        taskEntity.setTypos(task.getTypos());
        taskEntity.setStatus(task.getStatus());
        return taskEntity;
    }

    public Task toDTO(TaskEntity taskEntity) {
        Task task = new Task();
        task.setId(taskEntity.getId().toString());
        task.setInput(taskEntity.getInput());
        task.setPattern(taskEntity.getPattern());
        task.setPosition(taskEntity.getPosition());
        task.setTypos(taskEntity.getTypos());
        task.setStatus(taskEntity.getStatus());
        if (taskEntity.getStatus().equals(TaskStatus.PENDING)) {
            task.setProgress("0%");
        } else if (taskEntity.getStatus().equals(TaskStatus.COMPLETED)) {
            task.setProgress("100%");
        }
        return task;
    }

    public TaskEntity toEntity(TaskRequest taskRequest) {
        TaskEntity taskEntity = new TaskEntity();
        taskEntity.setInput(taskRequest.getInput());
        taskEntity.setPattern(taskRequest.getPattern());
        taskEntity.setStatus(TaskStatus.PENDING);
        return taskEntity;
    }
}
