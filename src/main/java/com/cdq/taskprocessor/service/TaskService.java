package com.cdq.taskprocessor.service;

import com.cdq.taskprocessor.model.Task;
import com.cdq.taskprocessor.model.TaskEntity;
import com.cdq.taskprocessor.model.TaskRequest;
import com.cdq.taskprocessor.service.consumer.TaskConsumerService;
import com.cdq.taskprocessor.service.finder.TaskFinderService;
import com.cdq.taskprocessor.service.producer.TaskProducerService;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class TaskService {

    private final TaskProducerService taskProducerService;
    private final TaskFinderService taskFinderService;
    private final TaskConsumerService taskConverter;

    public TaskService(TaskProducerService taskProducerService, TaskFinderService taskFinderService, TaskConsumerService taskConverter) {
        this.taskProducerService = taskProducerService;
        this.taskFinderService = taskFinderService;
        this.taskConverter = taskConverter;
    }

    public Task createTask(TaskRequest taskRequest) {
        TaskEntity taskEntity = taskProducerService.createInitialEntityStatus(taskRequest);
        return taskProducerService.sendTaskMessage(taskEntity);
    }

    public void processTask(Task task) {
        taskConverter.updateTaskEntityStatus(task);
        taskConverter.processTask(task);
        taskConverter.updateCompletedTaskEntity(task);
    }

    public List<String> getAllTaskIds() {
        return taskFinderService.getAllTaskIds();
    }

    public Optional<Task> getTaskById(String id) {
        return taskFinderService.getTaskById(id);
    }
}
