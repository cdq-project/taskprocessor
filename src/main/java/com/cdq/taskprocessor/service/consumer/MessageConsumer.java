package com.cdq.taskprocessor.service.consumer;

import com.cdq.taskprocessor.model.Task;
import com.cdq.taskprocessor.service.TaskService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class MessageConsumer {

    private final TaskService taskService;

    public MessageConsumer(TaskService taskService) {
        this.taskService = taskService;
    }

    @RabbitListener(queues = "${rabbitmq.queueName}")
    public void receiveMessage(Task task) {
        taskService.processTask(task);
    }
}
