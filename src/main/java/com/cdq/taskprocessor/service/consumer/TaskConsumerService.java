package com.cdq.taskprocessor.service.consumer;

import com.cdq.taskprocessor.model.Task;
import com.cdq.taskprocessor.model.TaskEntity;
import com.cdq.taskprocessor.model.TaskStatus;
import com.cdq.taskprocessor.model.converter.TaskConverter;
import com.cdq.taskprocessor.repository.TaskRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

@Service
public class TaskConsumerService {

    private final TaskConverter taskConverter;
    private final TaskRepository taskRepository;
    private final TaskPerformer taskPerformer;

    public TaskConsumerService(TaskConverter taskConverter, TaskRepository taskRepository, TaskPerformer taskPerformer) {
        this.taskConverter = taskConverter;
        this.taskRepository = taskRepository;
        this.taskPerformer = taskPerformer;
    }

    @Transactional
    public void updateTaskEntityStatus(Task task) {
        Optional<TaskEntity> optionalTaskEntity = taskRepository.findById(UUID.fromString(task.getId()));
        optionalTaskEntity.ifPresent(entity -> entity.setStatus(TaskStatus.IN_PROGRESS));
    }

    public void processTask(Task task) {
        task.setStatus(TaskStatus.IN_PROGRESS);
        task = taskPerformer.processTask(task);
        task.setStatus(TaskStatus.COMPLETED);
    }

    @Transactional
    public void updateCompletedTaskEntity(Task task) {
        TaskEntity updatedTaskEntity = taskConverter.toEntity(task);
        taskRepository.save(updatedTaskEntity);
    }
}
