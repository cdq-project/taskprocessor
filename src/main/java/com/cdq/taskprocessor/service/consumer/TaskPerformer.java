package com.cdq.taskprocessor.service.consumer;

import com.cdq.taskprocessor.model.Task;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class TaskPerformer {

    @Value("${task.sleepTime}")
    private int sleepTime;

    private final Map<String, Task> currentTasks = new HashMap<>();

    public Map<String, Task> getRunningTasks() {
        return currentTasks;
    }

    public Task processTask(Task task) {
        int repetitionsNumber = task.getInput().length() - task.getPattern().length() + 1;
        char[] patternChars = task.getPattern().toCharArray();
        task.setProgress("0%");
        currentTasks.put(task.getId(), task);

        sleepTime(sleepTime);
        for (int i=0; i < repetitionsNumber; i++) {
            int typosCounter = countTypos(task.getInput().substring(i, i + patternChars.length), patternChars);
            task.setProgress(computeProgress(i + 1, repetitionsNumber));
            sleepTime(sleepTime);
            if ((task.getPosition() == null && task.getTypos() == null) || typosCounter < task.getTypos()) {
                task.setPosition(i);
                task.setTypos(typosCounter);
            }
            if (typosCounter == 0) {
                break;
            }
        }

        task.setProgress("100%");
        currentTasks.remove(task.getId());
        return task;
    }

    private int countTypos(String input, char[] patternChars) {
        int typosCounter = 0;

        for (int i=0; i < input.length(); i++) {
            char[] inputChars = input.toCharArray();
            if (inputChars[i] != patternChars[i]) {
                typosCounter++;
            }
        }
        return typosCounter;
    }

    private String computeProgress(int position, int total) {
        return position * 100 / total + "%";
    }

    void sleepTime(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
