package com.cdq.taskprocessor.service.finder;

import com.cdq.taskprocessor.model.Task;
import com.cdq.taskprocessor.model.TaskEntity;
import com.cdq.taskprocessor.model.converter.TaskConverter;
import com.cdq.taskprocessor.repository.TaskRepository;
import com.cdq.taskprocessor.service.consumer.TaskPerformer;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class TaskFinderService {

    private final TaskRepository taskRepository;
    private final TaskPerformer taskPerformer;
    private final TaskConverter taskConverter;

    public TaskFinderService(TaskRepository taskRepository, TaskPerformer taskPerformer, TaskConverter taskConverter) {
        this.taskRepository = taskRepository;
        this.taskPerformer = taskPerformer;
        this.taskConverter = taskConverter;
    }

    public List<String> getAllTaskIds() {
        List<TaskEntity> tasks = taskRepository.findAll();
        return tasks.stream().map(task -> task.getId().toString()).toList();
    }

    public Optional<Task> getTaskById(String id) {
        Task runningTask = taskPerformer.getRunningTasks().get(id);
        if (runningTask != null) {
            return Optional.of(runningTask);
        }

        UUID taskId = null;
        try {
            taskId = UUID.fromString(id);
        } catch (Exception e) {
            return Optional.empty();
        }

        Optional<TaskEntity> taskEntity = taskRepository.findById(taskId);
        return taskEntity.map(taskConverter::toDTO);
    }
}
