package com.cdq.taskprocessor.service.producer;

import com.cdq.taskprocessor.model.Task;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class MessageProducer {

    @Value("${rabbitmq.queueName}")
    private String queueName;

    private final AmqpTemplate amqpTemplate;

    public MessageProducer(AmqpTemplate amqpTemplate) {
        this.amqpTemplate = amqpTemplate;
    }

    public void sendMessage(Task task) {
        amqpTemplate.convertAndSend(queueName, task);
    }
}
