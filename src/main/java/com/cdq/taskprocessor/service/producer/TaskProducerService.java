package com.cdq.taskprocessor.service.producer;

import com.cdq.taskprocessor.model.Task;
import com.cdq.taskprocessor.model.TaskEntity;
import com.cdq.taskprocessor.model.TaskRequest;
import com.cdq.taskprocessor.model.converter.TaskConverter;
import com.cdq.taskprocessor.repository.TaskRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TaskProducerService {

    private final MessageProducer messageProducer;
    private final TaskConverter taskConverter;
    private final TaskRepository taskRepository;

    public TaskProducerService(MessageProducer messageProducer,
                               TaskConverter taskConverter, TaskRepository taskRepository) {
        this.messageProducer = messageProducer;
        this.taskConverter = taskConverter;
        this.taskRepository = taskRepository;
    }

    @Transactional
    public TaskEntity createInitialEntityStatus(TaskRequest taskRequest) {
        TaskEntity taskEntity = taskConverter.toEntity(taskRequest);
        taskRepository.save(taskEntity);
        return taskEntity;
    }

    public Task sendTaskMessage(TaskEntity taskEntity) {
        Task task = taskConverter.toDTO(taskEntity);
        messageProducer.sendMessage(task);
        return task;
    }
}
