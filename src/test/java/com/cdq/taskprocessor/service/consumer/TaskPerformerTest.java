package com.cdq.taskprocessor.service.consumer;

import com.cdq.taskprocessor.model.Task;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class TaskPerformerTest {

    @Mock
    private Set<Task> currentTasksMock;

    @Spy
    @InjectMocks
    private TaskPerformer taskPerformer;

    @ParameterizedTest
    @CsvSource({
            "ABCD, BCD, 1, 0",
            "ABCD, BWD, 1, 1",
            "ABCDEFG, CFG, 4, 1",
            "ABCABC, ABC, 0, 0",
            "ABCDEFG, TDD, 1, 2"
    })
    void processTask(String input, String pattern, int expectedPosition, int expectedTypos) {
        Task task = createTestTask(input, pattern);

        when(currentTasksMock.add(any(Task.class))).thenReturn(false);
        when(currentTasksMock.remove(any(Task.class))).thenReturn(false);
        doNothing().when(taskPerformer).sleepTime(anyInt());

        Task resultTask = taskPerformer.processTask(task);

        assertAll(
                () -> assertThat(resultTask.getPosition()).isEqualTo(expectedPosition),
                () -> assertThat(resultTask.getTypos()).isEqualTo(expectedTypos)
        );
    }

    private Task createTestTask(String input, String pattern) {
        Task task = new Task();
        task.setInput(input);
        task.setPattern(pattern);
        return task;
    }
}